users=100
cd results-Altra
while [ $users -le 300 ]; do
        cnt=1
        while [ $cnt -le 5 ]; do
                p99=$(egrep "expected_response:true" VU-$users/$cnt.out | sed 's/\s[ ]*/,/g' | cut -d "," -f8 | cut -d "=" -f2)
                p100=$(egrep "expected_response:true" VU-$users/$cnt.out | sed 's/\s[ ]*/,/g' | cut -d "," -f10 | cut -d "=" -f2)
                Reqps=$(egrep "iterations..." VU-$users/$cnt.out | sed 's/\s[ ]*/,/g' | cut -d "," -f4)
                Reqs=$(egrep "expected_response:true" VU-$users/$cnt.out | sed 's/\s[ ]*/,/g' | cut -d "," -f11 | cut -d "=" -f2)
                Reqs_fail=$(egrep "http_req_failed" VU-$users/$cnt.out | sed 's/\s[ ]*/,/g' | cut -d "," -f5)


                echo "$p99,$p100,$Reqps,$Reqs,$Reqs_fail"
                cnt=$(( $cnt + 1 ))
        done
        echo ""
        users=$(( $users + 20 ))
done
cd ..
