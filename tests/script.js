import { sleep } from 'k6'
import { Rate } from 'k6/metrics'
import http from 'k6/http'

// let's collect all errors in one metric
let errorRate = new Rate('error_rate')

// See https://k6.io/docs/using-k6/options
export let options = {
  batch: 1,
  throw: true,
  stages: [
    { duration: '5m', target: 100 },
  ]
}

export default function () {
  let params = {
    headers: { 'X-CustomHeader': '1' },
  };
  // replace with your public ip
  let res = http.get('http://10.76.87.233', params)

  errorRate.add(res.status >= 400)

  sleep(1)
}

