users=100
while [ $users -le 100 ]; do
    cnt=1
    mkdir -p results-Altra/VU-$users
    sed -i -e "s/target: .*$/target: $users },/g" script.js
    while [ $cnt -le 1 ]; do
      k6 run --summary-trend-stats="min,avg,med,p(99),p(99.9),max,count" --summary-time-unit=s  script.js > results-Altra/VU-$users/$cnt.out
      sleep 30
      cnt=$(( $cnt + 1 ))
    done  
users=$(( $users + 250 ))
done

